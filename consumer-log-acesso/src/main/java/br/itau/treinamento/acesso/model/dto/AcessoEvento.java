package br.itau.treinamento.acesso.model.dto;


public class AcessoEvento {
	
	private Long portaId;
	private Long clienteId;
	private Boolean acesso;
	
	public Long getPortaId() {
		return portaId;
	}
	public void setPortaId(Long portaId) {
		this.portaId = portaId;
	}
	public Long getClienteId() {
		return clienteId;
	}
	public void setClienteId(Long clienteId) {
		this.clienteId = clienteId;
	}
	
	public Boolean getAcesso() {
		return acesso;
	}
	public void setAcesso(Boolean acesso) {
		this.acesso = acesso;
	}

}
