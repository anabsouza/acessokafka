package br.treinamento.itau.consumer;

import java.io.BufferedWriter;
import java.io.FileWriter;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import br.itau.treinamento.acesso.model.dto.AcessoEvento;

@Service
public class AcessoService {
	
    Logger logger = LoggerFactory.getLogger(AcessoService.class);

	
	public void criaEventoLog(AcessoEvento acessoEvento) {
		
		try {
			BufferedWriter writer =  new BufferedWriter(new FileWriter("./acesso-eventos.csv", true));	
			
			CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT);
	         		
			 csvPrinter.printRecord(
					 acessoEvento.getClienteId().toString(),
					 acessoEvento.getPortaId().toString(),
					 acessoEvento.getAcesso().toString());
			 
	         csvPrinter.flush();  
	         csvPrinter.close();
		}catch(Exception e) {
			logger.error("Erro ao criar evento", e);
		}
		 

	}

}
